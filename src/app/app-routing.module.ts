import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewsRoutingModule } from './views/views-routing.module';

const routes: Routes = [
  { path: '', redirectTo: 'search', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
  ViewsRoutingModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
