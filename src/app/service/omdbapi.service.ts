import { Injectable } from '@angular/core';

import { CustomHttp } from '../security/custom-http';
import { environment } from 'src/environments/environment';
import { MoviesList, SearchResult, Movie } from '../models/movies';


@Injectable({
  providedIn: 'root'
})
export class OmdbapiService {

  Url: string;
  apiKey: string;

  constructor(
    private http: CustomHttp,
  ) {
    this.Url = environment.ombUrl;
    this.apiKey = environment.apiKey;
  }

  getMoviesList(search: string): Promise<SearchResult> {
    return this.http.get<SearchResult>(`${this.Url}?s=${search}&apikey=${this.apiKey}`)
      .toPromise();
  }

  getMoviesById(id: string): Promise<Movie> {
    return this.http.get<Movie>(`${this.Url}?i=${id}&apikey=${this.apiKey}`)
      .toPromise();
  }

}
