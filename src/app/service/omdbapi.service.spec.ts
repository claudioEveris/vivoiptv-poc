import { TestBed } from '@angular/core/testing';

import { OmdbapiServiceService } from './omdbapi.service';

describe('OmdbapiServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OmdbapiServiceService = TestBed.get(OmdbapiServiceService);
    expect(service).toBeTruthy();
  });
});
