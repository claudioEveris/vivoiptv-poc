import { CardComponent } from './card/card.component';
import { SearchComponent } from './search/search.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ViewsRoutingModule } from './views-routing.module';
import { OmdbapiService } from '../service/omdbapi.service';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';

@NgModule({
  declarations: [
    MovieDetailComponent,
    SearchComponent,
    CardComponent],

  imports: [
    CommonModule,
    FormsModule,
    ViewsRoutingModule,
  ],
  providers: [OmdbapiService]
})
export class ViewsModule { }
