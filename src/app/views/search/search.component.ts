import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { OmdbapiService } from 'src/app/service/omdbapi.service';
import { ErrorHandlerService } from 'src/app/security/error-handler.service';
import { DataService } from 'src/app/service/data.service';

import { Movie, MoviesList } from 'src/app/models/movies';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  movie: Movie;

  message = '';
  moviesList: MoviesList[];
  selectedCard: MoviesList;

  selectedIndex = null;
  search: string;
  oldSearch: string;
  moviesQtde = 0;

  hasMovies = false;
  noResult = false;

  constructor(
    private service: OmdbapiService,
    private router: Router,
    private dataService: DataService,
    private errorHandler: ErrorHandlerService
  ) { }

  ngOnInit() {
    this.search = this.dataService.getSearch();
    if (this.search) {
      this.loadMoveisList();
    }
  }

  onEnter(event, form: FormControl) {
    switch (event.key) {
      case 'Enter':
        if ((this.selectedIndex === null || this.selectedIndex === undefined)  && this.search) {
          this.loadMoveisList();
          form.reset();
        } else {
          this.selectedCard = this.moviesList[this.selectedIndex];
          this.selectMovie(this.selectedCard);
        }
        break;
      case 'ArrowRight':
        this.selectedIndex == null ? this.selectedIndex = 0 : ++this.selectedIndex;
        if (this.selectedIndex > this.moviesList.length) {
          this.selectedIndex = 0;
        }
        break;
      case 'ArrowLeft':
        this.selectedIndex == null ? this.selectedIndex = 0 : --this.selectedIndex;
        if (this.selectedIndex < 0) {
          this.selectedIndex = this.moviesList.length;
        }
        break;
      case 'ArrowDown':
        this.selectedIndex == null ? this.selectedIndex = 0 : this.selectedIndex += 4;
        if (this.selectedIndex > this.moviesList.length) {
          this.selectedIndex = this.moviesList.length;
        }
        break;
      case 'ArrowUp':
        this.selectedIndex -= 4;
        if (this.selectedIndex < 0) {
          this.selectedIndex = 0;
        }
        break;
      default:
        break;
    }
  }

  private loadMoveisList() {
    this.selectedIndex = null;
    this.oldSearch = this.search;
    this.service.getMoviesList(this.search).
      then((result) => {
        this.moviesList = result.Search;
        this.moviesQtde = this.moviesList ? this.moviesList.length : 0;
        this.hasMovies = result.Response;
        if (this.moviesQtde > 0) {
          this.message = `${this.moviesQtde} resultados para '${this.oldSearch}'`;
        } else {
          this.message = `Nenhum resultado para '${this.oldSearch}'`;
        }
        this.search = null;
        this.dataService.setSearch(this.oldSearch);
      }).catch(erro => this.errorHandler.handle(erro));
  }

  selectMovie(card: MoviesList) {
    this.router.navigate([`/moviedetail/${card.imdbID}`]);
  }


}
