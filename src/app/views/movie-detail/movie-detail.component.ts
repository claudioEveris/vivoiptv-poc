import { Component, OnInit, ViewChild, ElementRef, Renderer } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Movie } from 'src/app/models/movies';
import { OmdbapiService } from 'src/app/service/omdbapi.service';
import { ErrorHandlerService } from 'src/app/security/error-handler.service';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  movie: Movie;

  @ViewChild('btn1', { static: false }) btn1: ElementRef;
  @ViewChild('btn2', { static: false }) btn2: ElementRef;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: OmdbapiService,
    private dataSAervice: DataService,
    private errorHandler: ErrorHandlerService,
    // tslint:disable-next-line: deprecation
    private renderer: Renderer
  ) { }

  ngOnInit() {
    const ombdId = this.route.snapshot.params.id;
    this.loadMovieById(ombdId);
  }

  loadMovieById(id: string) {
    this.service.getMoviesById(id).
      then((result) => {
        this.movie = result;
        this.renderer.invokeElementMethod(this.btn1.nativeElement, 'focus');
      }).catch(erro => this.errorHandler.handle(erro));


  }

  navigateBack() {
    this.router.navigate([`/search`]);
  }
  navigateToSearch() {
    this.dataSAervice.setSearch(null);
    this.router.navigate([`/search`]);
  }

  onkeyDown(event) {
    switch (event.key) {
      case 'ArrowRight':
      case 'ArrowDown':
        this.renderer.invokeElementMethod(this.btn1.nativeElement, 'focus');
        break;
      case 'ArrowLeft':
      case 'ArrowUp':
        this.renderer.invokeElementMethod(this.btn2.nativeElement, 'focus');
        break;
      default:
        break;
    }
  }

}
