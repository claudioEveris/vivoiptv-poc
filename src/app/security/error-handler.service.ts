import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor() { }

  handle(errorResponse: any) {
    let msg: string;

    if (typeof errorResponse === 'string') {
      msg = errorResponse;
    } else {
      if (errorResponse.error) {
        msg = errorResponse.error.message;
        console.error('Ocorreu um erro', errorResponse.message);
      } else {
        msg = 'Erro ao processar serviço remoto. Tente novamente';
      }
    }
//    alert(msg);
//    Swal.fire('Ops', msg, 'error');

  }

}
